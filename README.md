# notes-app api routes

### User login
POST /login
    request body: {password: string, login: string}

### User register
POST /register
    request body: {password: string, login: string}

### Get notes list
GET /notes

### Create new note
POST /notes
    request body: {title: string, text: string}

### Update note
PUT /notes
    request body: {_id: string, title: string, text: string}

### Create integration
POST /integration
    request body: {label: string, description: string}

### Update integration
PUT /integration
    request body: {_id: string, label: string, description: string}

### Get list of integrations
GET /integrations

