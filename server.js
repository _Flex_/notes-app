const initDB = require('./db').initDb;
const app = require('./app');

//TODO: in future this will be customizable
const port = 8000;

initDB((err, db) => {
    if (err) console.log(err);

    app.listen(port);
    console.log(`we are live on ${port}`);
    require('./app/routes')(app);
});

