const MongoClient = require('mongodb').MongoClient;
const db = require('./config/db');
const helpers = require('./app/helpers');
const app = require('./app');

let _db;
const mongoClient = new MongoClient(db.url, {useNewUrlParser: true});

function initDb(callback) {
    if (_db) {
        console.warn("Trying to init DB again!");
        return callback(null, _db);
    }
    mongoClient.connect((err, client) => {
        if (err) {
            return callback(err);
        }
        _db = client.db(db.name);
        require('./app/routes')(app, _db);
        helpers.init.startApp(_db);
        return callback(null, _db);
    });
}

function getDb() {
    return _db;
}

module.exports = {
    initDb,
    getDb
};