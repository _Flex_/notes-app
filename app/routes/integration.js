const UserController = require('../controllers/UserController');
const IntegrationController = require('../controllers/IntegrationController');

module.exports = function (app) {
    app.use(['/integrations', '/integration'], [UserController.authenticate, UserController.permission]);

    app.get('/integrations', IntegrationController.list);

    app.get('/integration/token', [IntegrationController.checkLabel, IntegrationController.token]);

    app.post('/integration', [IntegrationController.checkLabel, IntegrationController.create]);

    app.put('/integration', [IntegrationController.checkLabel, IntegrationController.update]);
};