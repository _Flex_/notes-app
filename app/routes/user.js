const UserController = require('../controllers/UserController');


module.exports = function (app) {
    app.post('/register', UserController.register);

    app.post('/login', UserController.login);

    app.get('/user', UserController.authenticate, UserController.getUser);
};
