const noteRoutes = require('./note');
const userRoutes = require('./user');
const intagrationRoutes = require('./integration');

module.exports = function (app) {
    noteRoutes(app);
    userRoutes(app);
    intagrationRoutes(app);
};