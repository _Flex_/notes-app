const NotesController = require('../controllers/NotesController');
const UserController = require('../controllers/UserController');

module.exports = function (app) {

    app.use('/notes', UserController.authenticate);

    app.post('/notes', NotesController.create);

    app.get('/notes', NotesController.list);

    app.put('/notes', NotesController.put);
};