const roles = require('../constants/auth').roles;
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const SECRET = require('../../config/auth').secret;
const db = require('../../db').getDb();
const _ = require('lodash');

class UserController {

    static async login(req, res) {
        const user = await db.collection('users').findOne({login: req.body.login});
        if (!user) {
            res.status(401).json({
                status: 'Incorrect password or login'
            });
            return;
        }

        if (bcrypt.compareSync(req.body.password, user.password)) {
            const result = {
                token: UserController.getToken({login: req.body.login, role: roles.USER}),
                login: user.login
            };
            res.send(result);
        } else {
            res.status(401).json({
                status: 'Incorrect password or login'
            });
        }
    }

    static async register(req, res) {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.password, salt);

        // TODO add validation requested fields
        const user = {
            login: req.body.login,
            password: hash,
            token: UserController.getToken({login: req.body.login, role: roles.USER}),
            role: roles.USER
        };

        const result = await db.collection('users').insertOne(user);
        res.send(result.ops[0]);
    }

    static async getUser(req, res) {
        res.send(req.user);
    }

    static getToken(user) {
        return jwt.sign({
            data: user,
            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
        }, SECRET);
    }

    static authenticate(req, res, next) {
        if (!(req.headers && req.headers.authorization)) {
            return res.status(400).json({
                status: 'Please log in'
            });
        }

        const db = require('../../db').getDb();
        const header = req.headers.authorization.split(' ');
        const token = header[1];

        jwt.verify(token, SECRET, (err, decoded) => {
            if (err) {
                return res.status(401).json({
                    status: 'Token is invalid'
                });
            }

            if (decoded.role === roles.USER) {
                return db.collection('users').findOne({login: decoded.data.login}, (err, user) => {
                    if (err) {
                        res.status(500).json({
                            status: 'error'
                        });
                    }
                    if (user) {
                        req.user = user;
                        next();
                    } else {
                        return res.status(401).json({
                            status: 'Unauthorized'
                        });
                    }
                })
            }

            req.user = decoded.data;
            next();
        });
    }

    static permission(req, res, next) {
        const role = _.get(req, 'user.role', null);
        if (role === roles.USER) {
             next();
        } else {
            return res.status(403).json({
                status: 'Permission Denied'
            });
        }
    }
}

module.exports = UserController;