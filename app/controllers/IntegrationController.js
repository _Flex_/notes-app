const db = require('../../db').getDb();
const jwt = require("jsonwebtoken");
const SECRET = require('../../config/auth').secret;
const roles = require('../constants/auth').roles;
const ObjectID = require('mongodb').ObjectID;
const _ = require('lodash');


class IntegrationController {

    static async list(req, res) {
        const collection = db.collection('integrations');
        const result = await collection.find().toArray();
        res.send(result);
    }

    static async token(req, res) {
        if (!req.body.label) {
            res.status(500).json({
                status: "Label is required field!"
            });
        }
        const collection = db.collection('integrations');

        const integration = await collection.findOne({label: req.body.label});

        if (integration) {
            res.send(integration.value.token);
        } else {
            res.status(404).json({
                status: 'Not Found'
            });
        }
    }

    static async create(req, res) {
        const token = IntegrationController.getToken(req.body);

        const integration = {...req.body, token: token, role: roles.INTEGRATION};

        const result = await db.collection('integrations').insertOne(integration);
        res.send(result.ops[0]);
    }

    static async update(req, res) {
        if (_.get(req, 'body._id')) {
            return res.status(404).json({
                status: "_id is required field"
            });
        }

        const collection = db.collection('integrations');
        const id = ObjectID(req.body._id);

        const integration = await collection.findOneAndUpdate({_id: id}, {
            $set: {...req.body}
        }, {returnOriginal: false});

        if (integration) {
            res.send(integration.value);
        } else {
            res.status(404).json({
                status: 'Not Found'
            });
        }
    }

    static getToken(data) {
        return jwt.sign({
            data: data,
            exp: Math.floor(Date.now() / 1000) + (3600 * 24 * 3650)
        }, SECRET);
    }

    static checkLabel(req, res, next) {
        if (!req.body.label) {
            return res.status(500).json({
                status: "Label is required field!"
            });
        }
        next();
    }
}

module.exports = IntegrationController;