const helpers = require("../helpers");
const db = require('../../db').getDb();
const ObjectID = require('mongodb').ObjectID;


class NotesController {

    static async create(req, res) {
        const collection = db.collection('notes');
        const id = await helpers.db.getNewNoteId(db);
        const note = {
            id: id,
            text: req.body.text,
            title: req.body.title
        };

        collection.insertOne(note, (err, result) => {
            if (err) {
                res.send(err);
            } else {
                res.send(result.ops[0]);
            }
        });
    }

    static async list(req, res) {
        const collection = db.collection('notes');
        const result = await collection.find().toArray();
        res.send(result);
    }

    static async put(req, res) {
        const collection = db.collection('notes');
        const id = ObjectID(req.body._id);
        const note = await collection.findOneAndUpdate({_id: id}, {
            $set: {
                title: req.body.title,
                text: req.body.text,
            }
        }, {returnOriginal: false});

        if (note) {
            res.send(note.value);
        } else {
            res.status(404).json({
                status: 'Not Found'
            });
        }
    }
}

module.exports = NotesController;