async function startApp(db) {
    await checkCollection('notes', db);
    await checkCollection('users', db);
    await checkCollection('counters', db);
    await checkCollection('integrations', db);
}

async function checkCollection(name, db) {
    const collections = await db.collections();
    if (!collections.map(c => c.s.name).includes(name)) {
        const collection = await db.createCollection(name);
        if (name === 'counters') {
            await createCounter(collection);
        }
    }
}

async function createCounter(collection) {
    return collection.insertOne({
        sequence_value: 0
    });
}

module.exports = {
  startApp
};