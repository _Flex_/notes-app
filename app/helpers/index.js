const db = require('./db');
const init = require('./init');

module.exports = {
    db,
    init
};