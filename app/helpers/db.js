async function getNewNoteId(db) {
    const counters = db.collection('counters');
    const sequenceDocument = await counters.findOneAndUpdate({}, {$inc: {sequence_value: 1}}, {returnOriginal: false});
    return sequenceDocument.value.sequence_value;
}

module.exports = {
    getNewNoteId
};